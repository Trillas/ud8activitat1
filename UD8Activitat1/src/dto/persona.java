package dto;

public class persona {
	//Doy los atributos de persona
	private String nombre;
	private int edad;
	private String dni;
	private final char SEXO;
	private double peso;
	private double altura;
	
	//Hago todos los constructores
	public persona (String nombre, int edad, String dni, char sexo, double peso, double altura) {
		this.nombre = nombre;
		this.edad = edad;
		this.dni = dni;
		this.SEXO = sexo;
		this.peso = peso;
		this.altura = altura;
	}
	
	public persona (String nombre, int edad, char sexo) {
		this.nombre = nombre;
		this.edad = edad;
		this.dni = "000000000A";
		this.SEXO = sexo;
		this.peso = 0;
		this.altura = 0;
	} 
	
	public persona () {
		this.nombre = "";
		this.edad = 0;
		this.dni = "000000000A";
		this.SEXO = 'H';
		this.peso = 0;
		this.altura = 0;
	}

	public String toString() {
		return "persona [nombre=" + nombre + ", edad=" + edad + ", dni=" + dni + ", SEXO=" + SEXO + ", peso=" + peso
				+ ", altura=" + altura + "]";
	}
}
